function Levels() {
	this.levels = {};
	this.reset();
};

Levels.prototype.reset = function() {
	this.levels = {
		0:	[
			new Person(-50, 0)
		],
		1:	[
			new Person(-50, 0),
			new Person(-100, 0)
		],
		2:	[
			new Person(-50, 0),
			new Person(-125, 0),
			new Person(-200, 0),
			new Person(-275, 0),
			new Person(-350, 0)
		],
		3:	[
			new Sasquatch(-50, 0),
			new Sasquatch(-125, 0),
			new Sasquatch(-200, 0),
			new Sasquatch(-275, 0),
			new Sasquatch(-350, 0)
		],
		4:	[
			new Sasquatch(-50, 0),
			new Sasquatch(-125, 0),
			new Sasquatch(-200, 0),
			new Sasquatch(-275, 0),
			new Giant(-350, 0),
			new Giant(-425, 0),
			new Giant(-500, 0)
		]
	};

	for (var a = 0 ; this.levels[a] ; ++a) {
		this.levels[a].push(new House(700, 0));
	}
};

Levels.prototype.get = function(level) {
	return this.levels[level];
};

Levels.prototype.check = function(entities) {
	for (var n = 0 ; n < entities.length ; ++n) {
		if (entities[n].npc === true) {
			return false;
		}
	}

	return true;
};