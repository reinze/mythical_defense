function Fire (x, y) {
	var canvas = document.getElementById('canvas');
	this.context = canvas.getContext('2d');
	this.x = x;
	this.y = y;
	this.width = 50;
	this.height = 50;
	this.diameter = 35;
	this.priority = 5;
	this.npc = false;
	this.hp = 35;
	this.strength = 9;
	this.attack = true;
};

Fire.prototype.move = function(x, y) {
	this.x = x;
	this.y = y;
};

Fire.prototype.draw = function() {
	this.x -= 6;
	this.y += Math.cos(this.x) * 5;

	this.context.beginPath();

	this.context.fillStyle = 'red';
	this.context.arc(this.x, this.y + this.height, this.diameter, 0, 360, 0);
	this.context.fill();
	this.context.fillStyle = 'black';

	this.context.closePath();
};