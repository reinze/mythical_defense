function Collision() {};

Collision.prototype.detect = function(entities) {
	for (var obj1 = 0 ; obj1 < entities.length ; ++obj1 ) {
		for (var obj2 = obj1 + 1 ; obj2 < entities.length ; ++obj2 ) {
			if (entities[obj1].x < entities[obj2].x + entities[obj2].width &&
			   entities[obj1].x + entities[obj1].width > entities[obj2].x &&
			   entities[obj1].y < entities[obj2].y + entities[obj2].height &&
			   entities[obj1].height + entities[obj1].y > entities[obj2].y) {
			    return [entities[obj1], entities[obj2]].sort(this.sort);
			}
		}
	}
	return false;
};

Collision.prototype.sort = function(a, b) {
	return a.priority < b.priority;
};

Collision.prototype.handle = function(entities) {
	var collided = this.detect(entities);

	if (collided === false || this.checkMoot(collided) === true) return false;

	// collided[0] == attacker
	// collided[1] == victim

	collided[1].hp -= collided[0].strength;
	this.assertHp(collided[1], entities);

	if (collided[0].attack === true) {
		collided[0].hp -= collided[0].strength;
		this.assertHp(collided[0], entities);
	}

};

Collision.prototype.assertHp = function(entity, entities) {
	if (entity.hp <= 0) {
		entities.splice(entities.indexOf(entity), 1)
	}
};

Collision.prototype.checkMoot = function(collided) {
	for (var n = 0 ; n < 2 ; ++n) {
		if ((collided[n].constructor.name === 'House' && collided[1 - n].npc === false) ||
		   	(collided[n].npc === true && collided[1 - n].npc === true) ||
			(collided[n].attack === true && collided[1 - n].attack === true)) {
				return true
			}
	}
};